// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import Axios from 'Axios'

//1. import component
import Goods from './components/Goods/Goods';
import Ratings from './components/Ratings/Ratings';
import Seller from './components/Seller/Seller';

Vue.config.productionTip = false

//install vue-router plugin
Vue.use(VueRouter);

//如果在其他組件中使用axios命令, 需要改寫為Vue原型屬性
Vue.prototype.$axios = Axios;

//2. 定義路由
const routes = [
  {
    path: '/',
    redirect: '/goods'
  },
  {
    path: '/goods',
    component: Goods
  },
  {
    path: '/ratings',
    component: Ratings
  },
  {
    path: '/seller',
    component: Seller
  }
]

//3. 創建router實例
const router = new VueRouter({
  routes,
  //選中後的類名
  linkActiveClass: 'active'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  //4.創建 and 掛載 實例
  router 
})
