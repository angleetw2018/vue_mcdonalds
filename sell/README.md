# 學習點
* npm install vue-router 無法使用
更新npm至6.14.8
``` bash
npm install -g npm@6.14.8
```

* 若vue value 有使用到Array類則需要加if判斷是否存在
``` javascript
<div v-if:"array">{{array[0].data}}</div>
```


* 邊界線圓形
```css
border-radius: 50%;
border: 1px soild #ababab
``` 

* Vue 轉場效過  

html:

```html
<transition name="xxx"></transition>
```
  
css:
```css
//進入
.xxx-enter //過渡開始的狀態
.xxx-enter-to //過渡結束的狀態
.xxx-enter-active 過渡時間,延遲,曲線函數

//離開
.xxx-leave //過渡開始的狀態
.xxx-leave-to //過渡結束的狀態
.xxx-enter-active //過渡時間,延遲,曲線函數

```

* 使用props傳值  

Star.vue (js部分)
```javascript
export default {
	props: {
		score: {
			type: Number
		}
	},
}
```
Header.vue(html部分)
```html
<Star :score='poiInfo.wm_poi_score'></Star>
```

Star.vue就可以拿到Header.vue的poiInfo.wm_poi_score    

* 懶得打一連串路徑  

在build/webpack.base.conf.js裡新增 components , 就能將resolve的路徑簡化
```javascript
resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
      //路徑配置
      'components': resolve('./src/components')
    },
  },
```

修改前:  
```javascript
import Star from "../components/Star/Star";
```
修改後:  
```javascript
import Star from "components/Star/Star";
```

* 用flex切版面  

```css
/* flex: flex-grow | flex-shrink | flex-basic;
flex-grow必選，其餘可選
默認: flex 0 1 auto ;
flex-grow: 定義放大比例，默認是0, 如果存在更多空間也不放大
flex-shrink:  定義縮小比例，默認是1，如果空間不足，該項目將會被縮小，flex-shrink屬性為0時，其他項目為1, 則空間不足時，前者不縮小;
flex-basic: 定義了在分配多餘空間之前，項目佔據主軸空間，瀏覽器根據這個屬性，計算佔據是否有多餘空間。
      默認值是auto，及項目本來大小，設為widtht屬性值一樣，則項目佔據固定空間;
*/  

.goods .menu-wrapper {
  flex: 0 0 85px;
  background: red;
}

.goods .food-wrapper {
  flex: 1;
  background: blue;
}
```

好用滾動套件 [better-scroll](https://github.com/ustbhuangyi/better-scroll):

``` shell
npm install better-scroll
```

* 用法參考 good.vue頁面  
* 使用時須注意vue的生命週期 ( 需再dom渲染完畢後使用 )

調整真機網頁時可以使用Weinre
``` shell
npm install -g weinre
```

weinre 執行command , 開啟網頁後，需再index.html加上weinre提供的script
``` shell
weinre --boundHost {{IP}} 
```