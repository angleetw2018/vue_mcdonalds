# sell

> 美國外賣APP

# Header
![image](https://gitlab.com/angleetw2018/vue_mcdonalds/-/raw/main/result/header.png)

# Header 按下活動後
![image](https://gitlab.com/angleetw2018/vue_mcdonalds/-/raw/main/result/header_bg.png)

# 商品頁
![image](https://gitlab.com/angleetw2018/vue_mcdonalds/-/raw/main/result/商品頁面.png)

# 商品頁-數據連動
![image](https://gitlab.com/angleetw2018/vue_mcdonalds/-/raw/main/result/商品數據連動.png)

# 評價頁
![image](https://gitlab.com/angleetw2018/vue_mcdonalds/-/raw/main/result/評價頁面.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
